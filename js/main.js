$(function(){

    // vars
    var htmlAndBodyElements = $('html, body'),
        navbarLink = $('.navbar-light .navbar-nav .nav-link'),
        fixed_menu = $('.fixed_menu').innerWidth();
    //End var
    $('.navbar-light .navbar-nav .nav-link').on('click',function(e){
        e.preventDefault();

        // $('body, html').animate(function(){
        //     console.log();
        // },1000);
        htmlAndBodyElements.animate({
            scrollTop: $( $(this).data('target') ).offset().top + 1
        },1000);
    });
    // add class active and remove from siblings
    navbarLink.on('click',function(){
        $(this).addClass('active').parent().siblings().find('a').removeClass('active');
    });
    ////////////////////////////////////////////
    $(window).scroll(function(){

    // sync navbar links with section
        $('.block').each(function(){
            if( $(window).scrollTop() > $(this).offset().top ){
                var blockId =  $(this).attr('id');
                $('.navbar-light .navbar-nav a').removeClass('active');
                $('.navbar-light .navbar-nav a[data-target="#' + blockId + '"]').addClass('active');
            }
        });
    ////////////////////////////////
    // Start feature csroll_to_top
    var csrollToTop = $('.csroll_to_top');
    if($(window).scrollTop() >= 300){
        
        if(csrollToTop.is(':hidden')){
            csrollToTop.fadeIn(1000);
        }

    }else{
        csrollToTop.fadeOut(1000);
    }
    csrollToTop.on('click',function(){
        htmlAndBodyElements.animate({
            scrollTop: 0
        },1000);
    });
    /////////////////////////////////
    ///////////// popUp ////////////
    });
    $('.show_popup').on('click',function(e){
        e.preventDefault();
        $($(this).data('popup')).fadeIn(1000);
 
    });
   $('.poup_up .inner_box button').on('click',function(e){
       e.preventDefault();
       $(this).parent().parent().fadeOut(1000);

   });
   $('.poup_up').on('click',function(){
       $(this).fadeOut(1000);
   });
   $('.poup_up .inner_box').on('click',function(e){
       e.stopPropagation();
   })
   $(document).on('keydown',function(e){
       if(e.keyCode == 27){
        $('.poup_up').fadeOut(1000);
       }
   });
   /////////////////////////////////////
   // button Effect 
   ////////// you can't open this code bocouse creat bgcolor by html dinimak
    // $('.button_effects .button_effect').each(function(){
    //     $(this).prepend('<span></span>');
    // });
   ////////////
   $('.effect_from_left,.effect_border_from_left').hover(function(){
       $(this).find('span').eq(0).animate({
           width:'100%'
       },1000);
   },function(){
    $(this).find('span').eq(0).animate({
        width:0
    },1000);
   });

    $('.effect_from_top,.effect_border_from_top').hover(function(){
    $(this).find('span').eq(0).animate({
        height:'100%'
    },1000);
    },function(){
    $(this).find('span').eq(0).animate({
        height:0
    },1000);
    });

   
/////////////////////////////
// animated progress
    $('.progress .progress-bar').each(function(){
        $(this).animate({
            width: $(this).attr('data-progress') + '%'
        },2000);
        $(this).text($(this).attr('data-progress') + '%');
    });
////////////////////////////////
// fixed menu
    $('.fixed_menu span').on('click',function(){
        $(this).parent('.fixed_menu').toggleClass('is_visvible');

        if($(this).parent('.fixed_menu').hasClass('is_visvible')){

            $(this).parent('.fixed_menu').animate({
                left:0
            },1000);

            $('body').animate({
                paddingLeft: fixed_menu
            },1000);

        }else{
            $(this).parent('.fixed_menu').animate({
                left: '-' + fixed_menu
            },1000);
            $('body').animate({
                paddingLeft: 0
            },1000);
        }

    });
    ///////////////////////////////////////
    // change web site colors
    $('.change_Color ul li').on('click',function(){
        $('body').attr('data-default-color',$(this).data('color'));
    });
    //////////////////////////////////////
    // Gallery
    var thumbnails_num_img              = $('.thumbnails_img').children().length,
        thumbnails_margin_img           = '.4',

        total_thumbnails_margin_img     = (thumbnails_num_img - 1) * thumbnails_margin_img,
        thumbnails_width_img            = (100 - total_thumbnails_margin_img) / thumbnails_num_img;

    $('.Gallery_iamge .thumbnails_img img').css({
        'width': thumbnails_width_img + '%',
        // 'marginRight' : thumbnails_margin_img + '%'
        // الكود اللي ملغي اللعنه عليك
    });
    
    $('.Gallery_iamge .thumbnails_img img').last().css({
        'margin-right': 0
    });

    $('.Gallery_iamge .thumbnails_img img').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.Gallery_iamge .master_img img').hide().attr('src',$(this).attr('src')).fadeIn(500);
    });
    $('.Gallery_iamge>.master_img .fa-chevron-right').on('click',function(){
        if($('.Gallery_iamge .thumbnails_img .active').is(':last-child')){
            $('.Gallery_iamge .thumbnails_img img').eq(0).click();
        }else{
            $('.Gallery_iamge .thumbnails_img .active').next().click();
        }
    });
    
    $('.Gallery_iamge>.master_img .fa-chevron-left').on('click',function(){
        if($('.Gallery_iamge .thumbnails_img .active').is(':first-child')){
            $('.Gallery_iamge .thumbnails_img img:last').click();
        }else{
            $('.Gallery_iamge .thumbnails_img .active').prev().click();
        }
       
    });
    // ////////////////////////////////////////////
    // products cards
    $('.products span').on('click',function(){
        $(this).children('svg').toggleClass('fa-plus fa-minus');
        $(this).next('p').slideToggle();
    });
    ////////////////////////////////////////////
    // items grid_view
    $('.items .change_vieo span').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.items').removeClass('list_view grid_view').addClass($(this).data('class'));
    });
    /////////////////////////////////////////////
    //Error Masseg
    $('.error_massage').each(function(){
        $(this).animate({
            right: 0
        },1000).delay(2000).animate({
            right: '-530px'
        });
    });
    ///////////////////////////////////////////
    // start Form Controll
    var placeAttr = '';
    $('[placeholder]').focus(function(){
        placeAttr = $(this).attr('placeholder');
        $(this).attr('placeholder','');
    }).blur(function(){
        $(this).attr('placeholder',placeAttr);
    });

    $('[required]').blur(function(){
        if($(this).val() == ''){
            $(this).next('small').fadeIn().delay(2000).fadeOut();
        }
    });
    /////////////////////////////////////////
    $('<span class="strerisk">*</span>').insertBefore(':input[required]');
    $('.strerisk').each(function(){
        $(this).css({
            'position':'absolute',
            'top': 41,
            'color': '#f00',
            'font-weight':'bold',
            'left':$(this).parent('div').find(':input').innerWidth() - 20
        });
    });
    /////////////////////////////////////////
    $('.form_controll form .form-group input[type="file"]').wrap('<div class="custom_file"></div>');
    $('.custom_file').prepend('<span>Upload Your File</span>');
    $('.custom_file').append('<i class="fa fa-upload"></i>');
    $('.form_controll form .form-group input[type="file"]').change(function(){
        $(this).prev('span').text($(this).val());
    });

    // Get the data_unicode
    $('.data_unicode').on('keyup',function(event){
        var keybordkey = event.keyCode ||event.which;
        $('.unicode').text(keybordkey);
    });
    //////////////////////////////
    // direction filde
    $('.form_controll form .form-group input').on('keyup',function(){
        if($(this).val().charCodeAt(0) < 200 ){
            $(this).css('direction','ltr');
        }else{
            $(this).css('direction','rtl');
        }
    });
    //////////////////////////////
    $('.form_controll form .form-group input.add_tags').on('keyup',function(event){

        var keybordkey2 = event.keyCode ||event.which;
        //console.log(keybordkey2);
         if(keybordkey2 === 188){

             var thisValue = $(this).val().slice(0,-1);
             $('.tags').append('<span class="tag_span"><i class="fa fa-times"></i>'+ thisValue +'</span>');

             $(this).val('');
         }
    });
    ///////////////////
    $('.tags').on('click','.tag_span svg',function(){
        $(this).parent('.tag_span').fadeOut(1000);
    });
    ///////////////////////////////////////
    // $('.about_us p.p_own').each(function(){
    //     //console.log( $(this).text().length );
    //     if($(this).text().length > 100){
    //         var trimmedText = $(this).text().substr(0,100);
    //         $(this).text(trimmedText + '...');
    //     }
    // });
    //////////////////////////////////
    // Start trimmed Text function
    function trimText(selectors,maxLength){
        $(selectors).each(function(){
            //console.log( $(this).text().length );
            if($(this).text().length > maxLength){
                var trimmedText = $(this).text().substr(0,maxLength);
                $(this).text(trimmedText + '...');
            }
        });
    }
    trimText('.about_us p.p_own',100);
    trimText('.about_us p.p_tow',150);
    // #######################################
    ///////////////////////////////////////
    // Strt Effect Bounce
    // $('.button_bounce').on('click',function(){
    //     $(this).animate({
    //         marginTop:'-=22px'
    //     }).animate({
    //         marginTop:'+=22px'
    //     });
    // });
    //////////////////
    // function effect_bounce(selectors,selectCss,valfirestAimate,secondAnimate){
    //     $(selectors).on('click',function(){
    //         $(this).animate({
    //             selectCss: valfirestAimate
    //         }).animate({
    //             selectCss: secondAnimate
    //         });
    //     });
    // }//Close function

    // effect_bounce('.button_bounce','margin-top','+=22px','-=22px');
    ////////////////////////////////
    function BounceElemnt(selector,times,destance,speed){
        for(var i = 0; i<times;i = i + 1){
            $(selector).animate({
                top:'-='+ destance
            },speed).animate({
                top:'+='+ destance
            },speed);
           
        }
    }
    $('.button_bounce').on('click',function(){
        BounceElemnt($(this),5,30,500);  
    });
    $('.button_bounce2').on('click',function(){
        BounceElemnt($(this),5,30,500);  
    });
    $('.button_bounce3').on('click',function(){
        BounceElemnt($(this),5,30,500);  
    });
    $('.button_bounce4').on('click',function(){
        BounceElemnt($(this),2,30,500);  
    });
    $('.button_bounce5').on('click',function(){
        BounceElemnt($(this),6,30,1000);  
    });
    ////////////////////////////////////////////////
    // height_div
    var height_div = 0;
    $('.height_div').each(function(){
        if($(this).height() > height_div){
            height_div = $(this).height();
        }
    });
    $('.height_div').height(height_div);
    ///////////////////////////////////////////
    // creat blink worning
    blinkWorning();
    function blinkWorning(){
        $('.functionSt').fadeOut(500,function(){
            $(this).fadeIn(500);
            blinkWorning();
        });
    }
    ///////////////////////////////////////
    //ToDo List
    var newTask = $('.addNew');

    $('.addTask').on('submit',function(e){
        e.preventDefault();
        if(newTask.val() != ''){
            $('<li>'+ newTask.val() +'</li>').appendTo('.tasks').fadeIn(500);
            newTask.val('');
        }
    });
    $('.tasks').on('click','li',function(){
        $(this).css('text-decoration','line-through').delay(2000).fadeOut(500,function(){
            $(this).remove();
        });
    });
    //////////////////////////////////////////////////////
    var theText = $('.type').data('text'),
        theTextLength = theText.length ,
        n = 0;

        theTyper = setInterval(function(){
            $('.type').each(function(){
                $(this).html($(this).html() + theText[n]);
            });
            n +=1;
            if(n >= theTextLength){
                clearInterval(theTyper);
            }
        },100);

        //////////////////////////////////////////
        // change_content
        (function change_content(){
            $('.change_content ul li.active').each(function(){
                if(!$(this).is(':last-child')){
                    $(this).delay(1000).fadeOut(1000,function(){
                        $(this).removeClass('active').next().addClass('active').fadeIn();
                        change_content(); 
                    });
                }else{
                    $(this).delay(1000).fadeOut(1000,function(){
                        $(this).removeClass('active');
                        $('.change_content ul li').eq(0).addClass('active').fadeIn();
                        change_content(); 
                    });   
                }
            })
        }());
        
});